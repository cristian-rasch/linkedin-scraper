require_relative "scraper/version"
require "nokogiri"
require "open-uri"
require "dotenv"
Dotenv.load
require "English"
require "set"
require "logger"

module LinkedIn
  class Scraper
    BASE_URL = "https://www.linkedin.com/job/:keywords-jobs/?sort=relevance".freeze
    EMPLOYMENT_TYPES = %w(Temporary Contract).freeze
    DEFAULT_VARIATIONS = ["remote", "telecommute", "telecommuting", "work from home", "contract", "freelance"].freeze
    DATE_FORMAT = "%b %d, %Y".freeze
    DEFAULT_LIMIT = 100
    MAX_PAGE = 25
    USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0".freeze

    class Proxy < Struct.new(:address, :port); end

    class Job < Struct.new(:title, :employer, :location, :created_on, :source, :url, :employment_type,
                           :experience, :job_function, :industry, :id, :description_html, :about_company_html)
    end

    # Options include:
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - max_page - the maximum number of pages to scrape per variation (defaults to 25, max. 25)
    #   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
    #   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the LINKED_IN_PROXIES env var or nil if missing
    #   - logger - a ::Logger instance to log error messages to
    def initialize(options = {})
      @options = options
    end

    # Options include:
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - max_page - the maximum number of pages to scrape per variation (defaults to 25, max. 25)
    #   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
    #   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/)
    #               defaults to reading them from the LINKED_IN_PROXIES env var or nil if missing
    #   - logger - a ::Logger instance to log error messages to
    #   - callback - an optional block to be called with each job found ASAP
    def job_search(keywords, options = {})
      configure_logger(options[:logger])
      configure_proxies(options[:proxies])

      limit = options[:limit] || @options[:limit] || DEFAULT_LIMIT
      limit = [limit, DEFAULT_LIMIT].min

      max_page = options[:max_page] || @options[:max_page] || MAX_PAGE
      offset = limit*-1

      variations = if options.has_key?(:variations)
                     options[:variations]
                   elsif @options.has_key?(:variations)
                     @options[:variations]
                   else
                     DEFAULT_VARIATIONS
                   end
      variations = Array(variations)
      variations << "" if variations.empty?

      keywords = Array(keywords).map(&:downcase)

      job_ids = Set.new
      discarded_job_ids = Set.new
      jobs = []
      current_page = 1

      catch(:done) do
        begin
          offset += limit
          search_url = BASE_URL.dup
          search_url << "&page_num=#{current_page}&trk=jserp_pagination_#{current_page}" if current_page > 1

          empty = variations.map do |variation|
            kws = keywords.dup
            kws << variation
            url = search_url.sub(":keywords", CGI.escape(kws.join(" ")))
            io = open_through_proxy(url)
            next(true) unless io

            jobs_page = Nokogiri::HTML(io)
            job_nodes = jobs_page.css(".job")

            if job_nodes.empty?
              true
            else
              job_nodes.each do |job_node|
                title_link = job_node.at_css("[itemprop=title]")
                job_url = title_link["href"]
                id = job_url[%r(/(\d+)\?), 1]
                next if discarded_job_ids.include?(id)

                title = title_link.text
                if company_name_node = job_node.at_css("[itemprop=name]")
                  employer = company_name_node.text
                else
                  employer = job_node.at_css("[itemprop=hiringOrganization]").text.strip
                end
                location = job_node.at_css("[itemprop=addressLocality]").text
                created_on_node = job_node.at_css("[itemprop=datePosted]")
                created_on = created_on_node.text
                source = nil
                if src_node = created_on_node.next_element
                  src = src_node.text
                  source = $POSTMATCH if src =~ /From /
                end

                io = open_through_proxy(job_url)
                next unless io

                job_page = Nokogiri::HTML(io)
                employment_type_node = job_page.at_css("[itemprop=employmentType]")
                employment_type = employment_type_node.text if employment_type_node
                unless EMPLOYMENT_TYPES.include?(employment_type)
                  discarded_job_ids << id
                  next
                end

                if !job_ids.include?(id) && job_ids.size < limit
                  job_ids << id
                  experience_node = job_page.at_css("[itemprop=experienceRequirements]")
                  experience = experience_node.text if experience_node
                  job_function_node = job_page.at_css("[itemprop=occupationalCategory]")
                  job_function = job_function_node.text if job_function_node
                  industry_node = job_page.at_css("[itemprop=industry]")
                  industry = industry_node.text if industry_node
                  description_html = job_page.at_css(".description-section [itemprop=description]").inner_html
                  about_company_node = job_page.at_css("#company-module [itemprop=description]")
                  about_company_html = about_company_node.inner_html if about_company_node

                  job = Job.new(title, employer, location, created_on, source, job_url,
                                employment_type, experience, job_function, industry, id,
                                description_html, about_company_html)
                  yield(job) if block_given?
                  jobs << job
                end

                throw(:done) if job_ids.size == limit
              end

              false
            end
          end

          done = empty.all?
          current_page += 1
          break if current_page > max_page
        end until done
      end

      jobs
    end


    private

    def configure_proxies(proxies = nil)
      @proxies = (proxies || @options[:proxies] || ENV.fetch("LINKED_IN_PROXIES").split(",")).shuffle
    end

    def configure_logger(logger = nil)
      @logger = logger || @options[:logger] || Logger.new(STDERR)
    end

    def open_through_proxy(url)
      begin
        open(url, "User-Agent" => USER_AGENT, "proxy" => next_proxy)
      rescue => err
        @logger.error "Cannot open '#{url}' - #{err.message}"
        nil
      end
    end

    def next_proxy
      @proxies.shift.tap { |proxy| @proxies << proxy }
    end

    def parse_date(date)
      Date.strptime(date, DATE_FORMAT)
    end
  end
end
