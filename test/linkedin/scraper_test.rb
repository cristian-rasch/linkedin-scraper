require File.expand_path("test/test_helper")
require File.expand_path("lib/linkedin/scraper")

module LinkedIn
  class ScraperTest < Minitest::Test
    def setup
      @linkedin = Scraper.new
      @keywords = "web developer"
    end

    def test_getting_one_job
      jobs = @linkedin.job_search(@keywords, limit: 1)
      assert_equal 1, jobs.size
    end

    def test_getting_three_jobs
      jobs = @linkedin.job_search(@keywords, limit: 3)
      assert_equal 3, jobs.size
    end

    def test_literal_search
      jobs = @linkedin.job_search(@keywords, variations: nil, limit: 1)
      assert_equal 1, jobs.size
    end

    def test_passing_in_a_callback_to_job_search
      job = nil
      @linkedin.job_search(@keywords, limit: 1) do |j|
        job = j
      end
      refute_nil job
    end
  end
end
