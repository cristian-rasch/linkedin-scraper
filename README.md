# LinkedIn::Scraper

LinkedIn jobs scraper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'linkedin-scraper', git: 'git@bitbucket.org:cristian-rasch/linkedin-scraper.git', require: 'linkedin/scraper'
```

And then execute:

    $ bundle

## Usage

```ruby
require "linkedin/scraper"
require "pp"

linkedin = LinkedIn::Scraper.new
# Options include:
#   - limit - how many jobs to retrieve (defaults to 100, max. 100)
#   - max_pages_count - the maximum number of pages to scrape per variation (defaults to 25, max. 25)
#   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
#   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888)
#               defaults to reading them from the LINKED_IN_PROXIES env var or nil if missing
#   - logger - a ::Logger instance to log error messages to
jobs = linkedin.job_search("web developer", limit: 3) # returns up to 3 jobs
pp jobs.first
# #<struct LinkedIn::Scraper::Job
#  title="Ruby Developer - Contract - MN - STAR #5777",
#  employer="STAR Collaborative",
#  location="Richfield",
#  created_on="Feb 13, 2015",
#  source=nil,
#  url="https://www.linkedin.com/jobs2/view/29057744?trk=jserp_job_details_text",
#  employment_type="Contract",
#  experience="Associate",
#  job_function="Information Technology",
#  industry="Computer Software, Information Technology and Services",
#  id="29057744",
#  description_html=
#   "<p><strong>Hello STAR Collaborative Network!</strong><br> <br>STAR #5777: Our client, a top national retailer, is looking for a self-directed, collaborative, Ruby Developer to help build deployment server for their Dotcom. This role will be collaboratively working with, reporting to, and taking direction from current Ruby Developer. Client will start the contractor earlier if they are available. </p>\n<p></p>\n<p><strong>*** To be considered for this role you must include a code sample of your previous work with your application.</strong><br> <br>If you are interested in the position, and if you feel you are qualified, please read our application FAQs (www.STARcollaborative.com/faq) then apply online. If this isn't a match for you, please forward on to anyone you think is interested and qualified! Pay it forward!<br> <br>Target Start Date: 3/2/2015<br> Target End Date:  1/31/2016<br> <br><strong>RESPONSIBILITIES</strong> </p>\n<ul>\n<li>Developing / Heads down developing </li>\n<li>Working in Paired Programming Environment </li>\n<li>Unit testing </li>\n<li>Quality Assurance of unit testing </li>\n<li>Deployments </li>\n<li>Work with infrastructure provisioning tools such as Chef and Vagrant </li>\n<li>Work with client/server models and queuing technologies (rabbitmq, redis, etc) </li>\n</ul>\n<p><br><strong>Want a bigger payday?</strong> Share STAR with your friends and colleagues and get an amazing referral bonus if we place them on a new opportunity! Get the details here: www.starcollaborative.com/referral<br> <br>Some clients require background checks and drug screening as a condition for engagement / employment. This means you could be asked to submit to any or all of the following: criminal background check, credit check or drug screen.<br> <br><strong>Candidates must be eligible to work in the United States without sponsorship.</strong> In addition, we are seeking local candidates as this role is not eligible for travel and relocation reimbursement.<br> <br>Attention Recruiters: We thank you for your interest, but we prefer to work directly with individuals and our own Collaborative Network on this posting. We will not review resumes/return calls from unsolicited recruiters.</p>",
#  about_company_html=
#   "<p>STAR Collaborative is a professional services staffing and search firm that provides high quality talent for a wide range of professions and industries. Our solutions range from direct hire search and placement to contract resources. We bring a unique perspective to the market in that we’ve been on your side of the table. We have been the internal staffing or recruiting person, project leaders and freelance consultants. We understand the business in a deep and personal way. Our experience inspired us to build a business to leverage our connections, disrupt the current industry paradigms, and create an abundance mentality that benefits everyone!<br> <br>Get to know STAR Collaborative!<br> &gt; Visit our website: www.STARcollaborative.com<br> &gt; Read our Values Manifesto: www.STARcollaborative.com/values<br> &gt; Like us on Facebook: www.facebook.com/STARcollaborative<br> &gt; Circle us on Google+: www.google.com/+STARcollaborative<br> &gt; Follow us on Twitter: www.twitter.com/JOBSatSTAR<br> &gt; Subscribe to our opportunity email: www.STARcollaborative.com/subscribe</p>">
# => #<struct LinkedIn::Scraper::Job title="Ruby Developer - Contract - MN - STAR #5777", employer="STAR Collaborative", location="Richfield", created_on="Feb 13, 2015", source=nil, url="https://www.linkedin.com/jobs2/view/29057744?trk=jserp_job_details_text", employment_type="Contract", experience="Associate", job_function="Information Technology", industry="Computer Software, Information Technology and Services", id="29057744", description_html="<p><strong>Hello STAR Collaborative Network!</strong><br> <br>STAR #5777: Our client, a top national retailer, is looking for a self-directed, collaborative, Ruby Developer to help build deployment server for their Dotcom. This role will be collaboratively working with, reporting to, and taking direction from current Ruby Developer. Client will start the contractor earlier if they are available. </p>\n<p></p>\n<p><strong>*** To be considered for this role you must include a code sample of your previous work with your application.</strong><br> <br>If you are interested in the position, and if you feel you are qualified, please read our application FAQs (www.STARcollaborative.com/faq) then apply online. If this isn't a match for you, please forward on to anyone you think is interested and qualified! Pay it forward!<br> <br>Target Start Date: 3/2/2015<br> Target End Date:  1/31/2016<br> <br><strong>RESPONSIBILITIES</strong> </p>\n<ul>\n<li>Developing / Heads down developing </li>\n<li>Working in Paired Programming Environment </li>\n<li>Unit testing </li>\n<li>Quality Assurance of unit testing </li>\n<li>Deployments </li>\n<li>Work with infrastructure provisioning tools such as Chef and Vagrant </li>\n<li>Work with client/server models and queuing technologies (rabbitmq, redis, etc) </li>\n</ul>\n<p><br><strong>Want a bigger payday?</strong> Share STAR with your friends and colleagues and get an amazing referral bonus if we place them on a new opportunity! Get the details here: www.starcollaborative.com/referral<br> <br>Some clients require background checks and drug screening as a condition for engagement / employment. This means you could be asked to submit to any or all of the following: criminal background check, credit check or drug screen.<br> <br><strong>Candidates must be eligible to work in the United States without sponsorship.</strong> In addition, we are seeking local candidates as this role is not eligible for travel and relocation reimbursement.<br> <br>Attention Recruiters: We thank you for your interest, but we prefer to work directly with individuals and our own Collaborative Network on this posting. We will not review resumes/return calls from unsolicited recruiters.</p>", about_company_html="<p>STAR Collaborative is a professional services staffing and search firm that provides high quality talent for a wide range of professions and industries. Our solutions range from direct hire search and placement to contract resources. We bring a unique perspective to the market in that we’ve been on your side of the table. We have been the internal staffing or recruiting person, project leaders and freelance consultants. We understand the business in a deep and personal way. Our experience inspired us to build a business to leverage our connections, disrupt the current industry paradigms, and create an abundance mentality that benefits everyone!<br> <br>Get to know STAR Collaborative!<br> &gt; Visit our website: www.STARcollaborative.com<br> &gt; Read our Values Manifesto: www.STARcollaborative.com/values<br> &gt; Like us on Facebook: www.facebook.com/STARcollaborative<br> &gt; Circle us on Google+: www.google.com/+STARcollaborative<br> &gt; Follow us on Twitter: www.twitter.com/JOBSatSTAR<br> &gt; Subscribe to our opportunity email: www.STARcollaborative.com/subscribe</p>">
```

